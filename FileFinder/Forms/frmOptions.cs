﻿using System.Windows.Forms;

namespace FileFinder
{
    public partial class frmOptions : Form
    {
        public frmOptions()
        {
            InitializeComponent();

            Icon = Properties.Resources.filefinder;
        }
    }
}
